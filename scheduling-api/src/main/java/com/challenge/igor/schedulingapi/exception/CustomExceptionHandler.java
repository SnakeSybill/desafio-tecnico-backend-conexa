package com.challenge.igor.schedulingapi.exception;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler {

	@ExceptionHandler({ PasswordsDontMatchException.class, InvalidDateException.class })
	public ResponseEntity<String> handlePasswordDontMatch(Exception ex) {
		return ResponseEntity.badRequest().body(ex.getMessage());
	}

	@ExceptionHandler({ UserNotFoundException.class, ScheduleNotFoundException.class })
	public ResponseEntity<String> UserNotFoundException(Exception ex) {
		return ResponseEntity.notFound().build();
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		String errorMessage = new ArrayList<>(ex.getAllErrors()).get(0).getDefaultMessage();
		return ResponseEntity.badRequest().body(errorMessage);
	}
}
