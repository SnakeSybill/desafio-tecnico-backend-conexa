package com.challenge.igor.schedulingapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challenge.igor.schedulingapi.domain.Schedule;

public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {

}
