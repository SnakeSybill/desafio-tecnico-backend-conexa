package com.challenge.igor.schedulingapi.service;

import com.challenge.igor.schedulingapi.domain.Schedule;
import com.challenge.igor.schedulingapi.domain.User;
import com.challenge.igor.schedulingapi.dto.ScheduleDto;

public interface ScheduleService {

	Schedule shedule(ScheduleDto dto, User doctor);
}
