package com.challenge.igor.schedulingapi.service;

import com.challenge.igor.schedulingapi.domain.Patient;
import com.challenge.igor.schedulingapi.dto.PatientDto;

public interface PatientService {
	Patient addPatient(PatientDto patientDto);
}
