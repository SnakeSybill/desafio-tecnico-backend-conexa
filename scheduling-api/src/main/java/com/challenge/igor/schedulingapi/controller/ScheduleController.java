package com.challenge.igor.schedulingapi.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.igor.schedulingapi.domain.User;
import com.challenge.igor.schedulingapi.dto.ScheduleDto;
import com.challenge.igor.schedulingapi.exception.UserNotFoundException;
import com.challenge.igor.schedulingapi.service.AuthService;
import com.challenge.igor.schedulingapi.service.ScheduleService;

@RestController
@RequestMapping(value = "/api/v1/attendance")
public class ScheduleController {

	@Autowired
	private ScheduleService service;
	@Autowired
	private AuthService authService;

	@PostMapping()
	public ResponseEntity<Void> schedule(@Valid @RequestBody ScheduleDto scheduleDto, HttpServletRequest request)
			throws UserNotFoundException {
		User doctor = authService.getLoggedUser(request);
		service.shedule(scheduleDto, doctor);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

}
