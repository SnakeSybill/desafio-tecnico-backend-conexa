package com.challenge.igor.schedulingapi.dto;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PatientDto {

	@JsonProperty("nome")
	@NotBlank(message = "Nome do paciente não deve ser vazio.")
	private String name;

	@CPF(message = "O CPF informado deve ser um CPF válido")
	private String cpf;
}
