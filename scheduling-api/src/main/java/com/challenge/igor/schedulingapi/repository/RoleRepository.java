package com.challenge.igor.schedulingapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challenge.igor.schedulingapi.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

	Role findByName(String name);
}
