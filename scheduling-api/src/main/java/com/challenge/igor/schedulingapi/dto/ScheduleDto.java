package com.challenge.igor.schedulingapi.dto;

import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ScheduleDto {

	@Future(message = "Data do agendamento deve ser futura à data atual")
	@NotNull(message = "Data e horário da consulta devem estar preenchidas.")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonProperty("dataHora")
	private LocalDateTime dateTime;

	@Valid
	@NotNull(message = "Dados do paciente não preenchidos")
	@JsonProperty("paciente")
	private PatientDto patient;

}
