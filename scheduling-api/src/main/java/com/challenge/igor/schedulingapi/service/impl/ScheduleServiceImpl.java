package com.challenge.igor.schedulingapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.igor.schedulingapi.domain.Patient;
import com.challenge.igor.schedulingapi.domain.Schedule;
import com.challenge.igor.schedulingapi.domain.User;
import com.challenge.igor.schedulingapi.dto.ScheduleDto;
import com.challenge.igor.schedulingapi.repository.ScheduleRepository;
import com.challenge.igor.schedulingapi.service.PatientService;
import com.challenge.igor.schedulingapi.service.ScheduleService;

@Service
public class ScheduleServiceImpl implements ScheduleService {

	@Autowired
	private ScheduleRepository repository;

	@Autowired
	private PatientService patientService;

	@Override
	public Schedule shedule(ScheduleDto dto, User doctor) {

		Patient patient = patientService.addPatient(dto.getPatient());

		Schedule schedule = new Schedule();
		schedule.setDateTime(dto.getDateTime());
		schedule.setDoctor(doctor);
		schedule.setPatient(patient);

		return repository.save(schedule);
	}
}
