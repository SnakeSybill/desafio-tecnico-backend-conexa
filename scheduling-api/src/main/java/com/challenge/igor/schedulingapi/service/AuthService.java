package com.challenge.igor.schedulingapi.service;

import javax.servlet.http.HttpServletRequest;

import com.challenge.igor.schedulingapi.domain.User;
import com.challenge.igor.schedulingapi.dto.UserDto;
import com.challenge.igor.schedulingapi.exception.PasswordsDontMatchException;
import com.challenge.igor.schedulingapi.exception.UserNotFoundException;

public interface AuthService {
	User signUpUser(UserDto userDto) throws PasswordsDontMatchException;

	User getByCpf(String cpf) throws UserNotFoundException;

	User getLoggedUser(HttpServletRequest request) throws UserNotFoundException;
	
	void logout(HttpServletRequest request);
}
