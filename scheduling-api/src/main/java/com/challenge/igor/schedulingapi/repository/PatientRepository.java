package com.challenge.igor.schedulingapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challenge.igor.schedulingapi.domain.Patient;

public interface PatientRepository extends JpaRepository<Patient, Integer> {

	Optional<Patient> findByCpf(String cpf);
}
