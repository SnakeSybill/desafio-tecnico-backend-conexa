package com.challenge.igor.schedulingapi.service.impl;

import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.igor.schedulingapi.domain.Patient;
import com.challenge.igor.schedulingapi.dto.PatientDto;
import com.challenge.igor.schedulingapi.repository.PatientRepository;
import com.challenge.igor.schedulingapi.service.PatientService;

@Service
public class PatientServiceImpl implements PatientService {

	@Autowired
	private PatientRepository repository;

	@Override
	public Patient addPatient(PatientDto patientDto) {
		Patient patient = findByCpf(patientDto);

		if(Objects.nonNull(patient))
			return patient;
		
		patient = new Patient();
		patient.setCpf(patientDto.getCpf());
		patient.setName(patientDto.getName());

		return repository.save(patient);
	}

	private Patient findByCpf(PatientDto patientDto) {
		Optional<Patient> patient = repository.findByCpf(patientDto.getCpf());

		if (patient.isPresent())
			return patient.get();
		
		return null;
	}

}
