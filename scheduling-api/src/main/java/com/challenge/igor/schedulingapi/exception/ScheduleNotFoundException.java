package com.challenge.igor.schedulingapi.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScheduleNotFoundException extends Exception {

	private static final long serialVersionUID = -7128773727992819999L;
	private String message;

	public ScheduleNotFoundException(String message) {
		this.message = message;
	}
}
