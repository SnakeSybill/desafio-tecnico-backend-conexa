package com.challenge.igor.schedulingapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.challenge.igor.schedulingapi.domain.Role;
import com.challenge.igor.schedulingapi.enums.RolesEnum;
import com.challenge.igor.schedulingapi.repository.RoleRepository;

@SpringBootApplication
public class SchedulingApiApplication implements CommandLineRunner {

	@Autowired
	private RoleRepository roleRepository;

	public static void main(String[] args) {
		SpringApplication.run(SchedulingApiApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Role doctorRole = new Role(null, RolesEnum.DOCTOR.name());
		roleRepository.save(doctorRole);
	}
}
