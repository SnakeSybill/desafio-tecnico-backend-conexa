package com.challenge.igor.schedulingapi.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class InvalidDateException extends Exception {

	private static final long serialVersionUID = -3854831705158703267L;

	private String message;
}
