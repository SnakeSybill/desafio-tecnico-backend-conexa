# process-manager
Api de agendamento de consultas

## Como executar
Primeiro, clone o projeto em sua máquina.

Para executar o projeto execute o comando
```
docker-compose up
```

Para testar a aplicação, importe no postman o arquivo `Scheduling API.postman_collection.json` e execute as chamadas (lembre-se de recuperar o token e atualizar o header 'Authorization' nas chamadas).
Para ver as tabelas do banco de dados acesse `http://http://localhost:8080/`.
